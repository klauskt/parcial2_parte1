﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parcial2_p1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        void change()
        {
            if ((trackBar1.Value >= trackBar1.Maximum / 2) && (trackBar2.Value >= trackBar2.Maximum / 2))
            {
                label1.Text = "warning";
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            label1.Text = trackBar1.Value.ToString();
            change();
        }

        private void trackBar2_ValueChanged(object sender, EventArgs e)
        {
            change();
        }
    }
}
